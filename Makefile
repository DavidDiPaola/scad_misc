# Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/).
# 2020-2022 David DiPaola.

SCAD = \
	alot.scad \
	arm.scad \
	case.scad \
	coord.scad \
	cube2.scad \
	cylinder_bent.scad \
	dict.scad \
	gear2.scad \
	hexagon.scad \
	matrix.scad \
	mscrew.scad \
	oval_fs.scad \
	pieslice.scad \
	trapezoid.scad \
	tube.scad \
	valueordefault.scad \
	wedge.scad

_CPPFLAGS = \
	-traditional-cpp -nostdinc -P \
	-CC \
	$(CPPFLAGS)

.PHONY: all
all: $(SCAD)

.PHONY: clean
clean:
	rm -f $(SCAD)

.PHONY: test
test:
	$(MAKE) CPPFLAGS=-DTEST

%.scad: %.cppscad
	cpp $(_CPPFLAGS) $< -o $@
