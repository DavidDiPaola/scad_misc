# scad_misc
Miscellaneous OpenSCAD helpers.

## Info
The source code uses the C preprocessor (cpp) to include dependencies.
Once the code is 'built' the resulting .scad files should be completely free-standing aside from dependencies on MCAD.

## Modules
- `arm` -- a `cube()` with `cylinder()`s and holes on each end
- `case` -- a rectangular enclosure with screw holes on each corner
- `cube2` -- a variant of `cube()` that also allows centering per-axis
- `cylinder_bent` -- a `cylinder()` but with a bend in it
- `gear2` -- an easier to use wrapper of MCAD's `gear()` module
- `hexagon` -- a hexagon
- `mscrew` -- metric screw modules and functions
- `oval_fs` -- an oval with flat sides
- `pieslice` -- a pie-like slice of a cylinder
- `trapezoid` -- a 2D trapezoid
- `trapezoid_prism` -- a 3D trapezoid
- `tube` -- a tube
- `wedge` -- a wedge shape

## Functions
- `coord` -- handy functions for manipulating coordinate values
- `dict` -- a rudimentary implementation of a dictionary data structure
- `matrix` -- handy functions for creating transformation matrixes
- `valueordefault` -- helper function that makes defaulting to a value easier

## Values
- `alot` -- really big numbers available in single number, 2D, and 3D variants

## Building
### How to build
`make -C <scad_misc dir>`
### Example Makefile which uses this library as a dependency
```
.PHONY: all
all: main.stl

.PHONY: scad_misc
scad_misc:
    $(MAKE) -C lib/scad_misc

main.stl: scad_misc
 
%.stl: %.scad
    openscad -o $@ $<
```

## License
Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/).

2020-2022 David DiPaola.
