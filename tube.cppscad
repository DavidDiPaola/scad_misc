#ifndef __TUBE_CPPSCAD
#define __TUBE_CPPSCAD

/*
Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/).
2020-2022 David DiPaola.
*/

// usage: use <tube.scad>

/*
Like cylinder() but hollow.
    od -- outer diameter
    od1,od2 -- lower and upper outer diameter, respectively
    id -- inner diameter
    id1,id2 -- lower and upper inner diameter, respectively
    h -- height
*/
module tube(
    od, od1, od2,
    id, id1, id2,
    h,
    center=false
) {
    _od1 = (od != undef) ? od : od1;
    _od2 = (od != undef) ? od : od2;
    _id1 = (id != undef) ? id : id1;
    _id2 = (id != undef) ? id : id2;
    if (_od1 == undef) {
        echo("ERROR: neither od or od1 was specified");
        assert(false);
    }
    if (_od2 == undef) {
        echo("ERROR: neither od or od2 was specified");
        assert(false);
    }
    if (_id1 == undef) {
        echo("ERROR: neither id or id1 was specified");
        assert(false);
    }
    if (_id2 == undef) {
        echo("ERROR: neither id or id2 was specified");
        assert(false);
    }
    
    difference() {
        cylinder(d1=_od1, d2=_od2, h=h, center=center);
        
        cylinder(d1=_id1, d2=_id2, h=h, center=center);
    }
}
//$fn=32; tube(od=10, id=8, h=5);
//$fn=32; tube(od=10, id=8, h=5, center=true);

#endif
